﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6.Models
{
        public class ExamContext : DbContext
        {
            public ExamContext(DbContextOptions options) : base(options)
            {

            }

            public DbSet<Product> Products { get; set; }
            public DbSet<Category> Categories { get; set; }

        }
}
